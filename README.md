# Docract

Racket documentation extraction tool

## About

Docract is a tool that helps the developers create and document their
applications written in the Racket programming language and with the use of
it's specific ecosystem.

Docract is a documentation extraction tool made in a similar fashion to
the well-known Doxygen.
Docract extracts function documentation and context from specially prepared
comments in the Racket/Scheme source code.
The extracted documentation is outputted in the Racket-native Scribble language
and can be consumed by the `scribble` tool to create a final API description
document.

Current major version of docract is 1.
Source code of his version resides inside the `Source/v1` directory.

## Features

* extraction of special comments from Racket source code,
* generating Scribble code from extracted blocks,
* a support [Scribble](https://docs.racket-lang.org/scribble/)
  library for special Docract forms,
* optional preview server that serves HTML pages produced from
  Scribble code that has been automatically extracted

## Installation

To install Docract you need a working installation of
[Racket](https://racket-lang.org/).

You can install with `req` using `cd Source/v1 && raco req -vA`
or install with `make` using `make -C Source/v1 install`.

## Examples

Inspect the contents of the `Source/v1/docract-examples` directory.
To run a given example use `make` on that directory,
for example `make -C Source/v1/docract-examples/usage/2023.07.02.23.20`.

## Documentation

Documentation can be browsed online on
[GitLab pages](https://xgqt.gitlab.io/xgqt-racket-app-docract/).

## License

Unless otherwise stated all source code in this repository is licensed under
the [MPL-2.0](https://opensource.org/license/mpl-2-0/) license.

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.
