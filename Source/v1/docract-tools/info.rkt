#lang info


(define pkg-desc "Racket documentation extraction tool. CLI Tools.")

(define version "0.0")

(define pkg-authors '(xgqt))

(define license 'MPL-2.0)

(define collection 'multi)

(define deps
  '("base"
    "threading-lib"
    "upi-lib"

    "docract-lib"))

(define build-deps
  '())
