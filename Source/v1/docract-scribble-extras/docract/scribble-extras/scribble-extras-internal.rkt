;; This Source Code Form is subject to the terms of the Mozilla Public
;; License, v. 2.0. If a copy of the MPL was not distributed with this
;; file, You can obtain one at http://mozilla.org/MPL/2.0/.

#lang racket/base

(require (only-in scribble/manual elem))

(provide (all-defined-out))

(define-syntax-rule (internal)
  (elem #:style 'smaller "Internal function"))
