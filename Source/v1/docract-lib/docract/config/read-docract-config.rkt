;; This Source Code Form is subject to the terms of the Mozilla Public
;; License, v. 2.0. If a copy of the MPL was not distributed with this
;; file, You can obtain one at http://mozilla.org/MPL/2.0/.

#|
#lang racket/base

(require file/glob)

(provide (all-defined-out))

;; anything goes...

(define docract-included-paths
  (list "some-path.rkt"
        (glob "mypkg-lib/*.rkt")
        (glob "mypkg-tools/support/*.rkt")))
|#

#lang racket/base

(require (only-in racket/list flatten))

(require (only-in threading ~>>))

(provide (all-defined-out))

#;{document-this
   @internal[]
   @brief{Parse and flatten a list of given paths to Racket source files.}

   @input[raw-list #:type list?]{list of files}
   @returns[#:type (listof path?)]{list of included Racket file paths}
   }
(define (docract-config-included-paths raw-list)
  (~>> raw-list
       flatten
       (map (lambda (maybe-path)
              (cond
                [(path? maybe-path)
                 maybe-path]
                [(string? maybe-path)
                 (string->path maybe-path)]
                [else
                 (error 'read-docract-config-file
                        "wrong type of file path, given: ~v"
                        maybe-path)])))
       (filter file-exists?)               ; Because user might not use "glob".
       (map resolve-path)))

#;{document-this
   @brief{Read a configuration file for Docract.}

   This function returns a hash of following values:

   @itemlist[
    @item{
     @racket['include-internal?] ---
     whether to include documentation marked as @racket[@internal[]]
     }
    @item{
     @racket['included-paths] ---
     list of Racket source files to extract documentation from
     }
    ]

   @input[config-file-path #:type path?]{config file to read}
   @returns[#:type hash?]{a special hash}
   }
(define (read-docract-config-file config-file-path)
  (define output-directory
    (dynamic-require config-file-path 'docract-output-directory (lambda () ".")))
  (define include-internal?
    (dynamic-require config-file-path 'docract-include-internal? (lambda () #false)))
  (define raw-included-paths
    (dynamic-require config-file-path 'docract-included-paths (lambda () '())))

  (hash 'output-directory output-directory
        'include-internal? include-internal?
        'included-paths (docract-config-included-paths raw-included-paths)))
