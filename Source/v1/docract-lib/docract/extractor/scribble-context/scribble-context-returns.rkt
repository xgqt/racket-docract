;; This Source Code Form is subject to the terms of the Mozilla Public
;; License, v. 2.0. If a copy of the MPL was not distributed with this
;; file, You can obtain one at http://mozilla.org/MPL/2.0/.


#lang racket/base

(require (only-in threading ~>> lambda~>>))

(provide (all-defined-out))


#;{document-this

   @input[context-string #:type string?]{context string}
   @returns[#:type string?]{extracted type}}
(define (get-scribble-context-returns context-string)
  (define string-returns
    (~>> context-string
         (regexp-split #rx"@|\n")
         (filter (lambda~>> (regexp-match #rx"returns\\[#:type .+\\]")))))

  (cond
    [(= (length string-returns) 1)
     (~>> string-returns
          car
          (regexp-match #rx"(?<=\\[#:type ).+(?=\\])")
          car)]
    [else
     "any"]))
