@defproc[
(port->augmented-racket [input-port input-port?])
list?
]{

   @internal[]

   @brief{
          Consume port contents and convert them following
          the "augmented-racket" syntax.
          }

   Used by @racket[port->scribble] from augmented-racket.

   @input[input-port #:type input-port?]{a port to consume}
   @returns[#:type list?]{a list of parsed datums}
}
@defproc[
(port->scribble [input-port input-port?])
string?
]{

   @internal[]

   @brief{Consume port contents and convert them to Scribble source code.}

   Uses @racket[port->augmented-racket] internally.

   @input[input-port #:type input-port?]{a port to consume}
   @returns[#:type string?]{Scribble source code}
}
