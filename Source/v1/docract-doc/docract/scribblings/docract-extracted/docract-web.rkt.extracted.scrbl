@defproc[
(run-docract-tools-main )
void?
]{

   @internal[]
   @brief{Run the "main" function from the "docract-tools" namespace.}

   @returns[#:type void?]{}
}
@defproc[
(make-scribble-entry [directory-path path-string?][included-file-names (listof path-string?)][extracted-files (listof path-string?)])
void?
]{

   @internal[]
   @brief{Create main Scribble entry file.}

   @input[directory-path #:type path-string?]{}
   @input[included-file-names #:type (listof path-string?)]{}
   @input[extracted-files #:type (listof path-string?)]{}
   @returns[#:type void?]{}
}
@defproc[
(run-scribble [directory-path path-string?])
void?
]{

   @internal[]
   @brief{Execute scribble in specified directory.}

   @input[directory-path #:type path-string?]{}
   @returns[#:type void?]{}
}
@defproc[
(included-name->output-path [included-path-name string?])
path?
]{

   @internal[]
   @brief{Return a path inside output directory.}

   @input[included-path-name #:type string?]{}
   @returns[#:type path?]{}
}
@defproc[
(included-name->tmp-path [included-path-name string?])
path?
]{

   @internal[]
   @brief{Return a path inside temporary directory.}

   @input[included-path-name #:type string?]{}
   @returns[#:type path?]{}
}
@defproc[
(regenerate-scribble-entry )
void?
]{

   @internal[]
   @brief{Regenerate the Scribbe page entries.}

   @returns[#:type void?]{}
}
