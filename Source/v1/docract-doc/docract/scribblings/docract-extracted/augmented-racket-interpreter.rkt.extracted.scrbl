@defproc[
(get-documentation-parts [datum-list list?])
(listof pair?)
]{

   @brief{
          Return proper parts of the extraction datum structure,
          such that they can be easily consumed.
          }

   Used by @racket[augmented-racket->scribble] from augmented-racket.

   @input[datum-list #:type list?]{datums}
   @returns[#:type (listof pair?)]{}
}
@defproc[
(augmented-racket->scribble [datum-list list?])
string?
]{

   @brief{
          Convert a list of datums produced by the parser to raw Scribble code.
          }

   Used by @racket[augmented-racket->scribble] from augmented-racket.

   @input[datum-list #:type list?]{datums}
   @returns[#:type string?]{Scribble code}
}
