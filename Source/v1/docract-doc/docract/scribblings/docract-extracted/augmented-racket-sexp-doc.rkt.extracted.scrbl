@defproc[
(get-expressson-body [expression-datum list?])
list?
]{

   @internal[]

   @brief{Return just the expression body from a list of datums.}

   @input[expression-datum #:type list?]{expression datum}
   @returns[#:type list?]{expression body}
}
@defproc[
(try-ref-doc-input [doc-inputs hash?][arg string?])
string?
]{

   @internal[]

   @brief{Attempt to find a @racketid[arg] in @racketid[doc-inputs] hash.}

   Raise a warning and return @racketid["any"] if it is not found.

   @input[doc-inputs #:type hash?]{}
   @input[arg #:type string?]{}

   @returns[#:type string?]{}
   
}
@defproc[
(format-doc-parsed-output [doc-inputs hash?][selected list?][result-accumulator string? ""])
string?
]{

   @internal[]

   @brief{Format object inputs into a string for outputting Scribble code.}

   This is a recursive function that accumulates it's output inside
   the @racket[result-accumulator] variable.

   @input[doc-inputs #:type hash?]{pairs of strings: identifier and it's contract}
   @input[selected #:type list?]{a list of selected identifiers used in object definition}
   @input[result-accumulator #:type string?]{a internal accumulator}

   @returns[#:type string?]{}
   
}
