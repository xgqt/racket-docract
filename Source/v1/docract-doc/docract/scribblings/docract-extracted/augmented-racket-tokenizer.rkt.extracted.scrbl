@defthing[
lex procedure?
]{

   @brief{Lexer of the "augmented-racket" syntax.}

   @returns[#:type procedure?]{lexer}
}
@defproc[
(tokenize [input-port input-port?])
list?
]{

   @brief{
          Run the "augmented-racket" tokenization process over the whole input
          port contents.
          }

   @input[input-port #:type input-port?]{}
   @returns[#:type list?]{datums}
}
