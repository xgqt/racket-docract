;; This Source Code Form is subject to the terms of the Mozilla Public
;; License, v. 2.0. If a copy of the MPL was not distributed with this
;; file, You can obtain one at http://mozilla.org/MPL/2.0/.

#lang scribble/manual

@require[@for-label[file/glob]]
@require[@for-label[racket/include]]
@require[@for-label[racket]]

@require[docract/scribble-extras]

@require[@for-syntax[@prefix-in[at-reader: @only-in[scribble/reader read-syntax]]]]
@require[@only-in[racket/include include/reader]]

@title[#:tag "docract"]{Docract}

@author[@author+email["Maciej Barć" "xgqt@gentoo.org"]]

Docract --- Racket documentation extraction tool.

@table-of-contents[]

@section{About}

Docract is a documentation extraction tool made to have a similar documentation
interface to the well-known @link["https://www.doxygen.nl/index.html"]{Doxygen}
tool.
Docract extracts function documentation and guesses a given object's context
from specially prepared comments in the source code.
The extracted documentation is outputted in the Racket-native Scribble language
and can be consumed by the @exec{scribble} tool to create a final API
description document in formats supported by Scribble, that is:
HTML, LaTex, MarkDown and paint text.

@section{Usage}

@subsection{Configuration file}

To configure how Docract works for your project a @exec{docract.config.rkt}
files is used.

The configuration file is written in the standard Racket language and should
export the following variables:

@itemlist[
 @item{@racketid[docract-included-paths]
  --- a @racket[list?] of file paths to search for documentation declarations,}
 @item{@racketid[docract-include-internal?]
  --- a @racket[boolean?] determining whether to include documentation parts
  marked as @racketid[@internal[]],}
 @item{@racketid[docract-output-directory]
  --- a directory @racket[path-string?] to output extracted contents into.}
 ]

A example Docract configuration file can look like this:

@codeblock|{
#lang racket/base
(require file/glob)
(provide docract-include-internal?)
(provide docract-included-paths)
(provide docract-output-directory)

(define docract-output-directory (build-path "scribblings/docract-extracted"))
(define docract-include-internal? #false)
(define docract-included-paths (list (glob "**/*.rkt")))
}|

@subsection{Previewing documentation}

To preview documentation use the @exec{docract-web} tool.
Docract-Web will read the @exec{docract.config.rkt} file just like
@exec{docract} but in addition to extraction it will also spawn a simple web
server that will be reloaded whenever changes to monitored files are detected.

@subsection{Extracting for use in scripts}

Use the @exec{docract} or the @exec{docract-page} tool.

Extracted documentation has to be loaded via @racket[include/reader].

Here is a example for loading extracted files:

@codeblock|{
#lang scribble/manual

@require[racket/include]
@require[@for-label[racket]]
@require[@for-syntax[@prefix-in[at-reader: scribble/reader]]]

@include/reader["file.rkt.extracted.scrbl" at-reader:read-syntax]
}|

@subsection{Documenting Racket code}

Currently those forms are supported:

@itemlist[
 @item{constant/variable}
 @item{macro (via @racket[define-syntax-rule])}
 @item{parameter}
 @item{procedure}
 @item{structure}
 ]

@subsubsection{Some examples of documenting Racket code}

Documenting this @racketid[boolean-parameter] parameter:

@codeblock|{
#lang racket

#;{document-this
   @input[b #:type boolean?]{}}
(define boolean-parameter
  (make-parameter #false))
}|

will produce:

@codeblock|{
#lang scribble/manual

@defparam[
boolean-parameter b boolean?
]{

   @input[b #:type boolean?]{}
}
}|

Documenting this @racketid[fib] procedure:

@codeblock|{
#lang racket

#;{document-this
   @brief{Fibonacci sequence function}

   Fibonacci sequence function that takes one parameter without using
   tail-call optimization.

   @input[n #:type int?]{index in the Fibonacci sequence}
   @returns[#:type int?]{value of the n-th Fibonacci number}}
(define (fib n)
  (cond
    [(<= n 1) n]
    [else (+ (fib (- n 1)) (fib (- n 2)))]))
}|

will produce:

@codeblock|{
#lang scribble/manual

@defproc[
(fib [n int?])
int?
]{

   @brief{Fibonacci sequence function}

   Fibonacci sequence function that takes one parameter without using
   tail-call optimization.

   @input[n #:type int?]{index in the Fibonacci sequence}
   @returns[#:type int?]{value of the n-th Fibonacci number}
}
}|

Documenting this @racketid[simple-struct] struct:

@codeblock|{
#lang racket

#;{document-this
   @input[a #:type boolean?]{}
   @input[b #:type int?]{}
   @input[c #:type string?]{}}
(struct simple-struct (a b c)
  #:transparent)
}|

will produce:

@codeblock|{
#lang scribble/manual

@defstruct[
simple-struct
([a boolean?][b int?][c string?])
]{

   @input[a #:type boolean?]{}
   @input[b #:type int?]{}
   @input[c #:type string?]{}
}
}|

@section{Internal APIs}

Docract does not provide a Racket API for public use, only console tools.

For people who do not wish to contribute to Docract development this section
could be treated as a showcase of what Scribble code generated by Docract looks
like in a HTML format.

@include/reader["./docract-extracted/augmented-racket-converter.rkt.extracted.scrbl" at-reader:read-syntax]
@include/reader["./docract-extracted/augmented-racket-interpreter.rkt.extracted.scrbl" at-reader:read-syntax]
@include/reader["./docract-extracted/augmented-racket-parser.rkt.extracted.scrbl" at-reader:read-syntax]
@include/reader["./docract-extracted/augmented-racket-sexp-doc.rkt.extracted.scrbl" at-reader:read-syntax]
@include/reader["./docract-extracted/augmented-racket-tokenizer.rkt.extracted.scrbl" at-reader:read-syntax]
@include/reader["./docract-extracted/docract-web.rkt.extracted.scrbl" at-reader:read-syntax]
