;; This Source Code Form is subject to the terms of the Mozilla Public
;; License, v. 2.0. If a copy of the MPL was not distributed with this
;; file, You can obtain one at http://mozilla.org/MPL/2.0/.

#lang racket/base

(require file/glob)

(provide docract-include-internal?
         docract-included-paths
         docract-output-directory)

(define docract-doc
  "./docract-doc/docract")

(define docract-lib
  "./docract-lib/docract")

(define docract-web
  "./docract-web/docract/web")

(define docract-output-directory
  (build-path docract-doc "scribblings/docract-extracted"))

(define docract-include-internal?
  #true)

(define docract-included-paths
  (list (glob (build-path docract-lib "**/augmented-racket-*.rkt"))
        (glob (build-path docract-web "**/docract-web.rkt"))))
