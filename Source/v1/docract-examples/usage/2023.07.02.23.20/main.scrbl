#lang scribble/manual

@require[@for-label[racket/base]]

@require[@for-label[docract/extractor/augmented-racket/augmented-racket-converter]]

@require[@for-syntax[@prefix-in[at: @only-in[scribble/reader read-syntax]]]]

@require[@only-in[racket/include include/reader]]

@require[docract/scribble-extras]


@title{Augmented Racket}

@section{Augmented Racket Converter}

@defmodule[docract/extractor/augmented-racket/augmented-racket-converter]

@include/reader["augmented-racket-converter.rkt.extracted.scrbl" at:read-syntax]
