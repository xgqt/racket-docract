# Prototype

## Parsers

### Augmented Racket parser

Initial parse via brag - custom parser for base Racket language,
do not discard comments with special syntax, that is `#;{}`,
keep whitespace in mind.

Tokenizer needs to discard:

* whitespace
* line comment
* block comments `#||#`

Refer to:

* guide/syntax-overview

```ebnf
epression ::= OPEN-PAREN atomic + CLOSE-PAREN
atomic ::= LITERAL | IDENTIFIER
```

### Context parser

Combines comments and nearest syntax that describen an entity.

Valid entities:

* define - lambda-style and shortened function syntax (CRITICAL)
* module
* struct

### Docract parser

Either go with brag or just throw comment contents to a eval module(?)
with extra Scribble definitions for Docract.

## Example

```racket
#;{document-this

   @brief{Fibonacci sequence function}

   The Fibonacci sequence function using one parameter without tail-call
   optimization.
   This is just a simple example for DocRact showcase.

   @input[n #:type any]{index in the Fibonacci sequence}}
(define (fib n)
  (cond
    [(<= n 1)
     n]
    [else
     (+ (fib (- n 1)) (fib (- n 2)))]))
```
